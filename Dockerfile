# build environment
FROM node:14.19.3 AS build
WORKDIR /usr/src/app
COPY ./ ./
RUN npm i
RUN npm run build

# production environment
FROM nginx:1.20-alpine
ENV FRONTEND_PORT=80
COPY --from=build /usr/src/app/build /var/www/html
WORKDIR /etc/nginx/conf.d
COPY ./nginx-server/http-server.conf.template ./
RUN rm -f default.conf
CMD ["/bin/sh","-c","envsubst '$$FRONTEND_PORT_HOSTNAME $$FRONTEND_PORT'< http-server.conf.template > http-server.conf && nginx -g 'daemon off;'"]
